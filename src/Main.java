
public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Création de deux fractions.");
		Fraction a = new Fraction(4,5);
		Fraction b = new Fraction(7,2);
		
		System.out.println();

		System.out.println("Consultations de la fraction a.");
		a.afficherNumerateurFraction();
		a.afficherDenominateurFraction();
		System.out.println("Valeur de la fraction a.");
		a.afficherValeurFraction();
		
		System.out.println();
		
		System.out.println("Consultations de la fraction b.");
		b.afficherNumerateurFraction();
		b.afficherDenominateurFraction();
		System.out.println("Valeur de la fraction b.");
		b.afficherValeurFraction();
		
		System.out.println();
		
		System.out.println("Addition des deux fractions (a+b).");
		Fraction res = a.addition(b);
		res.afficherNumerateurFraction();
		res.afficherDenominateurFraction();
		System.out.println("Valeur de l'addition (a+b).");
		res.afficherValeurFraction();
		
		System.out.println("Conversion toStringFraction()");
		System.out.println(res.toStringFraction());

	}

}
