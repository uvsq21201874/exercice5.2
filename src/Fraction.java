
public class Fraction{
	private int numerateur;
	private int denominateur;
	
	final static Fraction ZERO = new Fraction(0,1);
	final static Fraction UN = new Fraction(1,1);
	
	
//INITIALISATION	
	public Fraction(){ 
		this.numerateur=0;
		this.denominateur=1;
	}
		
	public Fraction(int num, int deno){
		this.numerateur=num;
		this.denominateur=deno;
	}
	
	public Fraction(int num){
		this.numerateur=num;
		this.denominateur=1;
	}
	
//CONSULTATION NUM ET DENO	
	public void afficherNumerateurFraction(){
		System.out.println(this.numerateur);
	}
	
	public void afficherDenominateurFraction(){
		System.out.println(this.denominateur);
	}
//CONSULTATION VALEUR	
	public void afficherValeurFraction(){
		double a = this.numerateur;
		double b = this.denominateur;
		System.out.println(a/b);
	}
//ADDITION
	public Fraction addition(Fraction b){
		if(this.denominateur==b.denominateur){
			return new Fraction(this.numerateur+b.numerateur,b.denominateur);
		}else{
			return new Fraction((this.numerateur*b.denominateur)+(b.numerateur*this.denominateur),this.denominateur*b.denominateur);
		}
	}
//COMPARAISON EGALITE
	public void egalite(Fraction b){
		if(this.denominateur==b.denominateur){
			if(this.numerateur==b.numerateur){
				System.out.println("Les fractions sont égales.");
			}else{
				System.out.println("Les fractions ne sont pas égales.");
			}
		}else{
			Fraction test1 = new Fraction(this.numerateur*b.denominateur,this.denominateur*b.denominateur);
			Fraction test2 = new Fraction(b.numerateur*this.denominateur,this.denominateur*b.denominateur);
			test1.egalite(test2);
		}
	}
//CONVERSION CHAINE DE CARACTERE
	public String toStringFraction(){
		String chaine = new String(this.numerateur+"/"+this.denominateur);
		return chaine;
	}
}
